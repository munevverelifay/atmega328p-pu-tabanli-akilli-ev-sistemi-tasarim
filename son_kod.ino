#include "LedControl.h"
#include <virtuabotixRTC.h>  
#include <OneWire.h> 
#include <DallasTemperature.h> 

#define ONE_WIRE_BUS 9 
OneWire oneWire(ONE_WIRE_BUS); 
DallasTemperature sensors(&oneWire);

LedControl lc = LedControl(11,13,10,1);  // Arduino Pins: (DIN:11, CLK:13, LOAD/CS:10, no.of devices is 1)


int CLK_PIN = 3;                                       //3. pini clock pini olarak tanımladık
int DAT_PIN = 2;                                       //2. pini data pini olarak tanımladık
int RST_PIN = 4;                                       //8. pini reset pini olarak tanımladık.
int manyetikdeger = 0;         
int buttondurum;                           
virtuabotixRTC myRTC(CLK_PIN, DAT_PIN, RST_PIN); 
String saat;
String dakika;
String sicaklik;
float santigrat = 30;


void display_set()
{
  char ekran[4] = {};       // max7219 modulunde 8 adet dipslay olduğu için 
  int index = 0;            // max7219u ayarlamak için kullanıldı
 
  for (int i = 0; i < String(sicaklik).length(); i++ )
  {
    
    
    char inChar = String(sicaklik)[i];        // tek bir byte oku
   
    switch(index)
    {
      case 0:                 // 1.karakter okunduğunda
   
        ekran[4] = ' ';       // değer yoksa display boş gözüksün
        ekran[3] = ' ';       // değer yoksa display boş gözüksün
        ekran[2] = ' ';       // değer yoksa display boş gözüksün
        ekran[1] = ' ';       // değer yoksa display boş gözüksün
        ekran[0] = inChar;
        break;
        
      case 1:                 // 2.karakter okunduğunda
    
        ekran[4] = ' ';       // değer yoksa display boş gözüksün
        ekran[3] = ' ';       // değer yoksa display boş gözüksün
        ekran[2] = ' ';       // değer yoksa display boş gözüksün
        ekran[1] = ekran[0];  // 1.okunan katakteri 2.displaye kaydır
        ekran[0] = inChar;
        break;
        
      case 2:                 // 3.karakter okunduğunda

        ekran[4] = ' ';       // değer yoksa display boş gözüksün
        ekran[3] = ' ';       // değer yoksa display boş gözüksün
        ekran[2] = ekran[1];  // 1.okunan katakteri 3.displaye kaydır
        ekran[1] = ekran[0];  // 2.okunan katakteri 2.displaye kaydır
        ekran[0] = inChar;
        break;
        
      case 3:                 // 4.karakter okunduğunda
   
        ekran[4] = ' ';       // değer yoksa display boş gözüksün
        ekran[3] = ekran[2];  // 1.okunan katakteri 4.displaye kaydır
        ekran[2] = ekran[1];  // 2.okunan katakteri 3.displaye kaydır
        ekran[1] = ekran[0];  // 3.okunan katakteri 2.displaye kaydır
        ekran[0] = inChar;
        break;
        
       case 4:                 // 5.karakter okunduğunda
    
        ekran[4] = ekran[3];  // 1.okunan katakteri 4.displaye kaydır
        ekran[3] = ekran[2];  // 2.okunan katakteri 3.displaye kaydır
        ekran[2] = ekran[1];  // 3.okunan katakteri 2.displaye kaydır
        ekran[1] = ekran[0];  // 4.okunan katakteri 1.displaye kaydır
        ekran[0] = inChar;
        break;
             
    }
    index++;
   }
     lc.setDigit(0,0,(String(ekran[0])).toInt(),false);
     lc.setDigit(0,1,(String(ekran[1])).toInt(),false); 
     lc.setDigit(0,2,(String(ekran[3])).toInt(),true);
     lc.setDigit(0,3,(String(ekran[4])).toInt(),false); 
     
  char goruntu[4] = {};       // max7219 modulunde 8 adet dipslay olduğu için 
  int artim = 0;            // max7219u ayarlamak için kullanıldı
 
  for (int j = 0; j < String(saat).length(); j++ )
  {
     
    char zaman = String(saat)[j];        // tek bir byte oku
   
    switch(artim)
    {
      case 0:                 // 1.karakter okunduğunda
 
        goruntu[4] = ' ';       // değer yoksa display boş gözüksün
        goruntu[3] = ' ';       // değer yoksa display boş gözüksün
        goruntu[2] = ' ';       // değer yoksa display boş gözüksün
        goruntu[1] = ' ';       // değer yoksa display boş gözüksün
        goruntu[0] = zaman;
        break;
        
      case 1:                 // 2.karakter okunduğunda

        goruntu[4] = ' ';       // değer yoksa display boş gözüksün
        goruntu[3] = ' ';       // değer yoksa display boş gözüksün
        goruntu[2] = ' ';       // değer yoksa display boş gözüksün
        goruntu[1] = goruntu[0];  // 1.okunan katakteri 2.displaye kaydır
        goruntu[0] = zaman;
        break;
        
      case 2:                 // 3.karakter okunduğunda
 
        goruntu[4] = ' ';       // değer yoksa display boş gözüksün
        goruntu[3] = ' ';       // değer yoksa display boş gözüksün
        goruntu[2] = goruntu[1];  // 1.okunan katakteri 3.displaye kaydır
        goruntu[1] = goruntu[0];  // 2.okunan katakteri 2.displaye kaydır
        goruntu[0] = zaman;
        break;
             
    }
    artim++;
  }

  lc.setDigit(0,6,(String(goruntu[0])).toInt(),true);
  lc.setDigit(0,7,(String(goruntu[1])).toInt(),false); 

  char goruntu2[4] = {};       // max7219 modulunde 8 adet dipslay olduğu için 
  int artim2 = 0;            // max7219u ayarlamak için kullanıldı
 
  for (int j = 0; j < String(dakika).length(); j++ )
  {
     
    char zaman = String(dakika)[j];        // tek bir byte oku
   
    switch(artim2)
    {
      case 0:                 // 1.karakter okunduğunda
 
        goruntu2[4] = ' ';       // değer yoksa display boş gözüksün
        goruntu2[3] = ' ';       // değer yoksa display boş gözüksün
        goruntu2[2] = ' ';       // değer yoksa display boş gözüksün
        goruntu2[1] = ' ';       // değer yoksa display boş gözüksün
        goruntu2[0] = zaman;
        break;
        
      case 1:                 // 2.karakter okunduğunda

        goruntu2[4] = ' ';       // değer yoksa display boş gözüksün
        goruntu2[3] = ' ';       // değer yoksa display boş gözüksün
        goruntu2[2] = ' ';       // değer yoksa display boş gözüksün
        goruntu2[1] = goruntu2[0];  // 1.okunan katakteri 2.displaye kaydır
        goruntu2[0] = zaman;
        break;
        
      case 2:                 // 3.karakter okunduğunda
 
        goruntu2[4] = ' ';       // değer yoksa display boş gözüksün
        goruntu2[3] = ' ';       // değer yoksa display boş gözüksün
        goruntu2[2] = goruntu2[1];  // 1.okunan katakteri 3.displaye kaydır
        goruntu2[1] = goruntu2[0];  // 2.okunan katakteri 2.displaye kaydır
        goruntu2[0] = zaman;
        break;
             
    }
    artim2++;
  }

  lc.setDigit(0,4,(String(goruntu2[0])).toInt(),false);
  lc.setDigit(0,5,(String(goruntu2[1])).toInt(),false); 


 }


void setup() {
   Serial.begin(9600); //Seri haberleşmeyi başlatıyoruz.
  myRTC.setDS1302Time(50, 59, 00, 4, 19, 9, 2019);
   sensors.begin();
   pinMode(7, OUTPUT);  //buzzer
   pinMode(6,OUTPUT);    //led    
   pinMode(5,INPUT);  //hall sensor
   pinMode(8,INPUT);   //button,switch

  lc.shutdown(0,false);                  // Enable display
  lc.setIntensity(0,3);                  // Set brightness level (0 is min, 15 is max)
  lc.clearDisplay(0);                    // Clear display register
}

void loop() {

   buttondurum = digitalRead(8);  //button kapalı mı açık mı kontrol ediyoruz
   manyetikdeger = digitalRead(5);     //5. pinden gelen değeri okuyoruz
   
   display_set();

   
  Serial.print(" Requesting temperatures...");
  sensors.requestTemperatures(); // Send the command to get temperatures
  Serial.println("DONE");

  Serial.print("Temperature is: ");
  Serial.print(sensors.getTempCByIndex(0)); // Why "byIndex"? 
    // You can have more than one IC on the same bus. 
    // 0 refers to the first IC on the wire
    delay(1000);
  sicaklik = sensors.getTempCByIndex(0);

  if(manyetikdeger == 1)            
  {
   digitalWrite(6,HIGH);
   Serial.println(" ");
   Serial.println("Kapı Açık");
             
   if(buttondurum == 0){               
   tone(7,300);
    }                                //eğer kapı açıksa led yansın ve button kapanana kadarda buzzer çalsın
     else{
      noTone(7);
      }
    
    }  
  else{
      noTone(7);
      digitalWrite(6,LOW);
      Serial.println(" ");
      Serial.println("Kapı Kapalı");
     }
   


  myRTC.updateTime();                                   //RTC'den zamanı okuyoruz

 Serial.print("Tarih / Saat: ");                       //Aldığımız verileri Serial Porta bastırıyoruz.
 Serial.print(myRTC.dayofmonth);
 Serial.print("/");
 Serial.print(myRTC.month);
 Serial.print("/");
 Serial.print(myRTC.year);
 Serial.print(" ");
 Serial.print(myRTC.hours);
 Serial.print(":");
 Serial.print(myRTC.minutes);
 Serial.print(":");
 Serial.println(myRTC.seconds);
 Serial.println(saat);
 Serial.println(dakika);
 saat = myRTC.hours; 
 dakika= myRTC.minutes;




























 

 }
